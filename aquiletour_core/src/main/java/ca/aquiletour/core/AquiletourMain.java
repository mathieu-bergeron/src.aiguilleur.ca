// Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
//
// This file is part of tutoriels4f5
//
// tutoriels4f5 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// tutoriels4f5 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with aquiletour.  If not, see <https://www.gnu.org/licenses/>

package ca.aquiletour.core;


import ca.aquiletour.core.messages.AddStudentCsvMessage;
import ca.aquiletour.core.messages.queue.UpdateIsQueueOpenMessage;
import ca.aquiletour.core.messages.queue.UpdateQueueInfoMessage;
import ca.aquiletour.core.messages.user.RenameUserMessage;
import ca.aquiletour.core.messages.user.ToggleAdminModeMessage;
import ca.aquiletour.core.messages.user.ToggleStudentModeMessage;
import ca.aquiletour.core.messages.user.UpdateUserInfoMessage;
import ca.aquiletour.core.messages.user.UserChangesPasswordMessage;
import ca.aquiletour.core.messages.user.UserInitiatesLoginMessage;
import ca.aquiletour.core.messages.user.UserIsActiveMessage;
import ca.aquiletour.core.messages.user.UserLogsOutMessage;
import ca.aquiletour.core.messages.user.UserSendsLoginCodeMessage;
import ca.aquiletour.core.messages.user.UserSendsPasswordMessage;
import ca.aquiletour.core.models.common.StoredListOfIds;
import ca.aquiletour.core.models.paths.CoursePath;
import ca.aquiletour.core.models.paths.TaskPath;
import ca.aquiletour.core.models.session.SessionData;
import ca.aquiletour.core.models.user.Admin;
import ca.aquiletour.core.models.user.Guest;
import ca.aquiletour.core.models.user.Student;
import ca.aquiletour.core.models.user.StudentGuest;
import ca.aquiletour.core.models.user.Teacher;
import ca.aquiletour.core.models.user.TeacherGuest;
import ca.aquiletour.core.models.user.User;
import ca.aquiletour.core.models.user_list.UserIdMap;
import ca.aquiletour.core.models.user_list.UserList;
import ca.aquiletour.core.models.user_session.SessionsByUserId;
import ca.aquiletour.core.models.user_uuid.UserIdByUuid;
import ca.aquiletour.core.models.user_uuid.UuidByUserId;
import ca.aquiletour.core.pages.home.ShowHomeMessage;
import ca.aquiletour.core.pages.queue.messages.ModifyAppointmentDurationsMessage;
import ca.aquiletour.core.pages.queue.messages.ModifyAppointmentTimesMessage;
import ca.aquiletour.core.pages.queue.models.Appointment;
import ca.aquiletour.core.pages.queue.models.AppointmentById;
import ca.aquiletour.core.pages.queue.models.StoredDate;
import ca.aquiletour.core.pages.queue.models.StoredAppointements;
import ca.aquiletour.core.pages.queue.models.StoredTags;
import ca.aquiletour.core.pages.queue.models.StoredTaskPath;
import ca.aquiletour.core.pages.queue.models.QueueModel;
import ca.aquiletour.core.pages.queue.models.QueueSettings;
import ca.aquiletour.core.pages.queue.models.QueueSettingsCourse;
import ca.aquiletour.core.pages.queue.models.SettingsByCourseKey;
import ca.aquiletour.core.pages.queue.models.SettingsByGroupId;
import ca.aquiletour.core.pages.queue.models.StoredCoursePath;
import ca.aquiletour.core.pages.queue.student.messages.AddAppointmentMessage;
import ca.aquiletour.core.pages.queue.student.messages.ModifyAppointmentCommentMessage;
import ca.aquiletour.core.pages.queue.teacher.messages.DeleteAppointmentMessage;
import ca.aquiletour.core.pages.queue.teacher.messages.MoveAppointmentMessage;
import ca.aquiletour.core.pages.queue_list.models.QueueListItem;
import ca.aquiletour.core.pages.queue_list.models.QueueListItemById;
import ca.aquiletour.core.pages.queue_list.models.QueueListModel;
import ca.aquiletour.core.pages.root.RootController;
import ca.ntro.core.Path;
import ca.ntro.core.mvc.ControllerFactory;
import ca.ntro.core.mvc.NtroContext;
import ca.ntro.core.mvc.NtroWindow;
import ca.ntro.core.system.log.Log;
import ca.ntro.core.system.trace.T;
import ca.ntro.core.tasks.NtroTaskSync;
import ca.ntro.messages.MessageHandler;
import ca.ntro.messages.ntro_messages.NtroUpdateSessionMessage;
import ca.ntro.messages.ntro_messages.UpdateSocketStatusMessage;
import ca.ntro.services.Ntro;
import ca.ntro.services.NtroInitializationTask;
import ca.ntro.users.NtroSession;

public abstract class AquiletourMain extends NtroTaskSync {
	
	private static boolean isSocketOpen = false;
	
	public static void setIsSocketOpen(boolean isSocketOpen) {
		AquiletourMain.isSocketOpen = isSocketOpen;
	}

	public static NtroContext<User, SessionData> createNtroContext() {
		T.call(AquiletourMain.class);

		NtroContext<User, SessionData> context = new NtroContext<>();

		context.updateIsSocketOpen(isSocketOpen);
		context.registerLang(Constants.LANG); // TODO

		context.registerUser((User) Ntro.currentUser());

		if(Ntro.currentSession().getSessionData() instanceof SessionData) {
			context.registerSessionData((SessionData) Ntro.currentSession().getSessionData());
		}else {
			context.registerSessionData(new SessionData());
		}
		
		return context;
	}
	
	public static String currentCategoryId() {
		T.call(AquiletourMain.class);
		
		SessionData sessionData = (SessionData) Ntro.currentSession().getSessionData();
		
		String currentCategoryId = sessionData.getCurrentCategoryId();
		
		if(currentCategoryId == null || currentCategoryId.isEmpty()) {
			currentCategoryId = Constants.CATEGORY_ID_CURRENT;
			setCurrentCategoryId(currentCategoryId);
		}
		
		return sessionData.getCurrentCategoryId();
	}

	public static void setCurrentCategoryId(String categoryId) {
		T.call(AquiletourMain.class);
		
		SessionData sessionData = (SessionData) Ntro.currentSession().getSessionData();

		sessionData.setCurrentCategoryId(categoryId);
		
		// XXX: saves session in cookie in JSweet
		Ntro.sessionService().registerCurrentSession(Ntro.currentSession());
	}

	protected abstract void registerViewLoaders();
	
	protected abstract Path subControllersPath();

	@Override
	protected void runTask() {
		T.call(this);
		
		// FIXME
		Constants.LANG = getPreviousTask(NtroInitializationTask.class, "initializationTask").getOption("lang");
		Constants.LANG = "fr";

		registerViewLoaders();

		// XXX: "/**" means: execute every subController
		// XXX: "/*/*/*" means: execute every subController down 3 levels
		// XXX: "/settings/*" means: execute the settings controller, then subController of settings
		RootController rootController = ControllerFactory.createRootController(RootController.class, subControllersPath(), getWindow(), createNtroContext());  

		rootController.execute();
		
		Ntro.backendService().handleMessageFromBackend(NtroUpdateSessionMessage.class, new MessageHandler<NtroUpdateSessionMessage>() {
			@Override
			public void handle(NtroUpdateSessionMessage message) {
				T.call(this);

				NtroSession session = message.getSession();
				Ntro.sessionService().registerCurrentSession(session); // XXX: saves cookie
				
				rootController.changeContext(AquiletourMain.createNtroContext());
			}
		});

		Ntro.messages().registerHandler(UpdateSocketStatusMessage.class, new MessageHandler<UpdateSocketStatusMessage>() {
			@Override
			public void handle(UpdateSocketStatusMessage message) {
				T.call(this);
				
				AquiletourMain.setIsSocketOpen(message.getIsSocketOpen());
				NtroContext<?,?> context = AquiletourMain.createNtroContext();
				
				rootController.changeContext(context);
			}
		});
		
		Ntro.messages().registerHandler(ToggleStudentModeMessage.class, new MessageHandler<ToggleStudentModeMessage>() {
			@Override
			public void handle(ToggleStudentModeMessage message) {
				T.call(this);
				
				NtroSession session = Ntro.currentSession();
				User user = (User) session.getUser();

				if(user instanceof Teacher) {

					Teacher teacher = (Teacher) user;
					teacher.toggleStudentMode();
					
					session.setUser(teacher);
					Ntro.sessionService().registerCurrentSession(session); // XXX: saves cookie

					rootController.changeContext(AquiletourMain.createNtroContext());
				}
				
				Ntro.backendService().sendMessageToBackend(message);
			}
		});

		Ntro.messages().registerHandler(ToggleAdminModeMessage.class, new MessageHandler<ToggleAdminModeMessage>() {
			@Override
			public void handle(ToggleAdminModeMessage message) {
				T.call(this);
				
				NtroSession session = Ntro.currentSession();
				User user = (User) session.getUser();

				if(user instanceof Admin) {

					Admin admin = (Admin) user;
					admin.toggleAdminMode();
					
					session.setUser(admin);
					Ntro.sessionService().registerCurrentSession(session); // XXX: saves cookie

					rootController.changeContext(AquiletourMain.createNtroContext());
				}

				Ntro.backendService().sendMessageToBackend(message);
			}
		});
	}
	
	public static void registerSerializableClasses() {
		T.call(AquiletourMain.class);

		Ntro.registerSerializableClass(QueueModel.class);
		Ntro.registerSerializableClass(StoredAppointements.class);
		Ntro.registerSerializableClass(Appointment.class);

		Ntro.registerSerializableClass(QueueListModel.class);
		Ntro.registerSerializableClass(QueueListItem.class);

		Ntro.registerSerializableClass(CoursePath.class);

		Ntro.registerSerializableClass(User.class);
		Ntro.registerSerializableClass(Teacher.class);
		Ntro.registerSerializableClass(TeacherGuest.class);
		Ntro.registerSerializableClass(Student.class);
		Ntro.registerSerializableClass(StudentGuest.class);
		Ntro.registerSerializableClass(Admin.class);
		Ntro.registerSerializableClass(Guest.class);
		Ntro.registerSerializableClass(SessionData.class);

		Ntro.registerSerializableClass(AddAppointmentMessage.class);
		Ntro.registerSerializableClass(DeleteAppointmentMessage.class);
		Ntro.registerSerializableClass(MoveAppointmentMessage.class);

		Ntro.registerSerializableClass(UserInitiatesLoginMessage.class);
		Ntro.registerSerializableClass(UserLogsOutMessage.class);

		Ntro.registerSerializableClass(UserSendsLoginCodeMessage.class);
		Ntro.registerSerializableClass(AddStudentCsvMessage.class);

		Ntro.registerSerializableClass(UpdateUserInfoMessage.class);

		Ntro.registerSerializableClass(StoredDate.class);
		Ntro.registerSerializableClass(StoredTags.class);

		Ntro.registerSerializableClass(ModifyAppointmentDurationsMessage.class);
		Ntro.registerSerializableClass(ModifyAppointmentTimesMessage.class);
		Ntro.registerSerializableClass(ToggleStudentModeMessage.class);

		Ntro.registerSerializableClass(UserList.class);
		Ntro.registerSerializableClass(UuidByUserId.class);
		Ntro.registerSerializableClass(UserIdByUuid.class);
		Ntro.registerSerializableClass(UserIdMap.class);

		Ntro.registerSerializableClass(TaskPath.class);

		Ntro.registerSerializableClass(QueueSettings.class);
		Ntro.registerSerializableClass(QueueSettingsCourse.class);
		Ntro.registerSerializableClass(SettingsByCourseKey.class);
		Ntro.registerSerializableClass(SettingsByGroupId.class);

		Ntro.registerSerializableClass(UpdateIsQueueOpenMessage.class);

		Ntro.registerSerializableClass(SessionsByUserId.class);

		Ntro.registerSerializableClass(ToggleAdminModeMessage.class);


		Ntro.registerSerializableClass(ModifyAppointmentCommentMessage.class);

		Ntro.registerSerializableClass(UserChangesPasswordMessage.class);

		Ntro.registerSerializableClass(UpdateQueueInfoMessage.class);
		Ntro.registerSerializableClass(StoredCoursePath.class);
		Ntro.registerSerializableClass(StoredTaskPath.class);

		Ntro.registerSerializableClass(AppointmentById.class);
		Ntro.registerSerializableClass(StoredListOfIds.class);

		Ntro.registerSerializableClass(QueueListModel.class);
		Ntro.registerSerializableClass(QueueListItemById.class);

		Ntro.registerSerializableClass(UserIsActiveMessage.class);

		Ntro.registerSerializableClass(UpdateSocketStatusMessage.class);

		Ntro.registerSerializableClass(UserSendsPasswordMessage.class);

		Ntro.registerSerializableClass(RenameUserMessage.class);

		Ntro.registerSerializableClass(ShowHomeMessage.class);

	}
	
	protected abstract NtroWindow getWindow();

	@Override
	protected void onFailure(Exception e) {
		Log.error("[FATAL] Initialization error");
		e.printStackTrace(System.err);
	}
}
