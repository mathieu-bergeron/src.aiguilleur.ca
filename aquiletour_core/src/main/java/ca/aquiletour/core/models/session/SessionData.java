package ca.aquiletour.core.models.session;


import ca.aquiletour.core.Constants;
import ca.ntro.core.system.trace.T;
import ca.ntro.users.NtroSessionData;

public class SessionData extends NtroSessionData {

	private int failedPasswordAttemps = 0;
	private String loginCode = "";
	private String currentCategoryId = Constants.CATEGORY_ID_CURRENT;

	public String getLoginCode() {
		return loginCode;
	}

	public void setLoginCode(String loginCode) {
		this.loginCode = loginCode;
	}

	public String getCurrentCategoryId() {
		return currentCategoryId;
	}

	public void setCurrentCategoryId(String currentCategoryId) {
		this.currentCategoryId = currentCategoryId;
	}

	public int getFailedPasswordAttemps() {
		return failedPasswordAttemps;
	}

	public void setFailedPasswordAttemps(int failedPasswordAttemps) {
		this.failedPasswordAttemps = failedPasswordAttemps;
	}

	public void incrementFailedPasswordAttemps() {
		T.call(this);
		
		failedPasswordAttemps++;
	}

	public void resetFailedPasswordAttemps() {
		T.call(this);
		
		failedPasswordAttemps = 0;
	}
	
	public boolean hasReachedMaxPasswordAttemps() {
		T.call(this);
		
		return failedPasswordAttemps >= Constants.MAX_PASSWORD_ATTEMPS;
	}

	public SessionData toPublicData() {
		
		SessionData publicData = new SessionData();
		
		publicData.setFailedPasswordAttemps(getFailedPasswordAttemps());
		publicData.setCurrentCategoryId(getCurrentCategoryId());

		return publicData;
	}

	@Override
	protected void deepCopyOf(NtroSessionData toCopy) {
		T.call(this);
		
		super.deepCopyOf(toCopy);
		
		if(toCopy instanceof SessionData) {
			
			SessionData sessionToCopy = (SessionData) toCopy;
			
			setFailedPasswordAttemps(sessionToCopy.getFailedPasswordAttemps());
			setCurrentCategoryId(sessionToCopy.getCurrentCategoryId());
			setLoginCode(getLoginCode());
		}
	}
}
