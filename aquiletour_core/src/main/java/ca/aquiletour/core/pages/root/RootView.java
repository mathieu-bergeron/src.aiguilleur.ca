package ca.aquiletour.core.pages.root;

import java.util.List;

import ca.aquiletour.core.pages.home.HomeView;
import ca.aquiletour.core.pages.login.LoginView;
import ca.aquiletour.core.pages.queue.views.QueueView;
import ca.aquiletour.core.pages.queue_list.views.QueueListView;
import ca.ntro.core.mvc.NtroContext;
import ca.ntro.core.mvc.NtroView;
import ca.ntro.messages.NtroMessage;

public interface RootView extends NtroView {

	void onContextChange(NtroContext<?,?> context);
	
	void showQueue(Class<? extends NtroView> subViewClass, QueueView queueView);
	void showLogin(Class<? extends NtroView> subViewClass, LoginView loginView);
	void showQueues(Class<? extends NtroView> subViewClass, QueueListView currentView);
	void showHome(Class<? extends NtroView> subViewClass, HomeView homeView);

	void displayErrorMessage(String message);
	void displayPrimaryMessage(String message);
	void displayUserScreenName(String screenName);
	void showLoginMenu(String messageToUser, List<NtroMessage> delayedMessages);
	void showPasswordMenu();

}
