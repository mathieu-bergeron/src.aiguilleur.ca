package ca.aquiletour.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ca.aquiletour.core.Constants;
import ca.aquiletour.core.messages.queue.UpdateIsQueueOpenMessage;
import ca.aquiletour.core.messages.queue.UpdateQueueInfoMessage;
import ca.aquiletour.core.messages.user.ItsNotMeMessage;
import ca.aquiletour.core.messages.user.ToggleAdminModeMessage;
import ca.aquiletour.core.messages.user.ToggleStudentModeMessage;
import ca.aquiletour.core.messages.user.UpdateUserInfoMessage;
import ca.aquiletour.core.messages.user.UserChangesPasswordMessage;
import ca.aquiletour.core.messages.user.UserInitiatesLoginMessage;
import ca.aquiletour.core.messages.user.UserLogsOutMessage;
import ca.aquiletour.core.messages.user.UserSendsLoginCodeMessage;
import ca.aquiletour.core.messages.user.UserSendsPasswordMessage;
import ca.aquiletour.core.models.paths.CoursePath;
import ca.aquiletour.core.models.paths.TaskPath;
import ca.aquiletour.core.models.session.SessionData;
import ca.aquiletour.core.models.user.Admin;
import ca.aquiletour.core.models.user.Teacher;
import ca.aquiletour.core.models.user.User;
import ca.aquiletour.core.pages.queue.messages.ModifyAppointmentDurationsMessage;
import ca.aquiletour.core.pages.queue.messages.ModifyAppointmentTimesMessage;
import ca.aquiletour.core.pages.queue.student.messages.AddAppointmentMessage;
import ca.aquiletour.core.pages.queue.student.messages.ModifyAppointmentCommentMessage;
import ca.aquiletour.core.pages.queue.teacher.messages.DeleteAppointmentMessage;
import ca.aquiletour.core.pages.queue.teacher.messages.MoveAppointmentMessage;
import ca.ntro.backend.UserInputError;
import ca.ntro.core.Path;
import ca.ntro.core.mvc.NtroContext;
import ca.ntro.core.system.trace.T;
import ca.ntro.messages.NtroMessage;
import ca.ntro.services.Ntro;

public class AquiletourBackendRequestHandler {
	
	public static void sendMessages(NtroContext<User, SessionData> context, Path path, Map<String, String[]> parameters) throws UserInputError {
		T.call(AquiletourBackendRequestHandler.class);
		
		sendRootMessages(context, path, parameters);

		if(path.startsWith(Constants.QUEUE_LIST_URL_SEGMENT)) {
			
			sendQueueListMessages(path.subPath(1), parameters);

		}else if(path.startsWith(Constants.QUEUE_URL_SEGMENT)) {
			
			sendQueueMessages(path.subPath(1), parameters, context.user());

		}else if(path.startsWith(Constants.LOGIN_URL_SEGMENT)) {
			
			sendLoginMessages(path.subPath(1), parameters);
	
		}else if(path.startsWith(Constants.HOME_URL_SEGMENT)) {

			sendHomeMessages(path.subPath(1), parameters);

		}

	}

	public static void sendRootMessages(NtroContext<User, SessionData> context, Path path, Map<String, String[]> parameters) throws UserInputError {
		T.call(AquiletourBackendRequestHandler.class);

		if(parameters.containsKey("logout")) {
			
			Ntro.messages().send(Ntro.messages().create(UserLogsOutMessage.class));
			
		} else if(parameters.containsKey("loginStep01")
				&& parameters.containsKey("studentId")
				&& !parameters.get("studentId")[0].isEmpty()) {
			
			String userId = parameters.get("studentId")[0].trim();
			
			sendLoginMessage(userId, parameters);

		} else if(parameters.containsKey("loginStep01")
				&& parameters.containsKey("teacherId") 
				&& !parameters.get("teacherId")[0].isEmpty()) {
			
			String userId = parameters.get("teacherId")[0].trim();
			
			sendLoginMessage(userId, parameters);

		} else if(parameters.containsKey("itsNotMe")) {
			
			ItsNotMeMessage itsNotMeMessage = Ntro.messages().create(ItsNotMeMessage.class);
			itsNotMeMessage.setDelayedMessages(delayedMessages(parameters));
			Ntro.messages().send(itsNotMeMessage);

		} else if(parameters.containsKey("loginCode")) {
			
			String loginCode = null;
			String firstName = null;
			String lastName = null;

			if(parameters.containsKey("loginCode")) {
				loginCode = parameters.get("loginCode")[0];
			}

			if(parameters.containsKey("firstName")) {
				firstName = parameters.get("firstName")[0];
			}

			if(parameters.containsKey("lastName")) {
				lastName = parameters.get("lastName")[0];
			}

			UserSendsLoginCodeMessage userSendsLoginCodeMessage = Ntro.messages().create(UserSendsLoginCodeMessage.class);
			userSendsLoginCodeMessage.setLoginCode(loginCode);
			userSendsLoginCodeMessage.setFirstName(firstName);
			userSendsLoginCodeMessage.setLastName(lastName);
			userSendsLoginCodeMessage.setDelayedMessages(delayedMessages(parameters));
			Ntro.messages().send(userSendsLoginCodeMessage);

		} else if(parameters.containsKey("userName")) {
			
			String screenName = parameters.get("userName")[0];
			
			UpdateUserInfoMessage updateUserInfoMessage = Ntro.messages().create(UpdateUserInfoMessage.class);
			updateUserInfoMessage.setScreenName(screenName);
			Ntro.messages().send(updateUserInfoMessage); // XXX: must be Ntro.message(), in JSweet the frontend handles it

		} else if(parameters.containsKey("newPasswordA")) {

			String currentPassword = null;
			if(parameters.containsKey("currentPassword")) {
				currentPassword = parameters.get("currentPassword")[0];
			}
			
			String newPasswordA = parameters.get("newPasswordA")[0];
			String newPasswordB = parameters.get("newPasswordB")[0];
			
			UserChangesPasswordMessage userChangesPassword = Ntro.messages().create(UserChangesPasswordMessage.class);
			userChangesPassword.setCurrentPassword(currentPassword);
			userChangesPassword.setNewPasswordA(newPasswordA);
			userChangesPassword.setNewPasswordB(newPasswordB);
			
			Ntro.messages().send(userChangesPassword);

		} else if(parameters.containsKey("password")) {

			String password = parameters.get("password")[0];
			
			UserSendsPasswordMessage userSendsPassword = Ntro.messages().create(UserSendsPasswordMessage.class);
			userSendsPassword.setPassword(password);
			userSendsPassword.setDelayedMessages(delayedMessages(parameters));
			
			Ntro.messages().send(userSendsPassword);

		} else if(parameters.containsKey("toggleStudentMode")) {

			if(context.user() instanceof Teacher) {
				((Teacher) context.user()).toggleStudentMode();
			}

			ToggleStudentModeMessage toggleStudentModeMessage = Ntro.messages().create(ToggleStudentModeMessage.class);
			Ntro.messages().send(toggleStudentModeMessage);

		} else if(parameters.containsKey("toggleAdminMode")) {

			if(context.user() instanceof Admin) {
				((Admin) context.user()).toggleAdminMode();
			}

			ToggleAdminModeMessage toggleAdminModeMessage = Ntro.messages().create(ToggleAdminModeMessage.class);
			Ntro.messages().send(toggleAdminModeMessage);
		}
	}

	@SuppressWarnings("unchecked")
	private static List<NtroMessage> delayedMessages(Map<String, String[]> parameters) {
		T.call(AquiletourBackendRequestHandler.class);
		
		List<NtroMessage> delayedMessages = null;

		String delayedMessagesText = parameters.get("delayedMessages")[0];
		
		if(delayedMessagesText.isEmpty()) {
			
			delayedMessages = new ArrayList<>();
			
		}else {

			delayedMessagesText = delayedMessagesText.replaceAll("\\\"","\"");
			delayedMessages = (List<NtroMessage>) Ntro.jsonService().fromString(List.class, delayedMessagesText);
		}

		return delayedMessages;
	}

	private static void sendLoginMessage(String userId, Map<String, String[]> parameters) {
		T.call(AquiletourBackendRequestHandler.class);
		
		UserInitiatesLoginMessage userInitiatesLoginMessage = Ntro.messages().create(UserInitiatesLoginMessage.class);
		userInitiatesLoginMessage.setRegistrationId(userId);
		userInitiatesLoginMessage.setDelayedMessages(delayedMessages(parameters));
		Ntro.messages().send(userInitiatesLoginMessage);
	}


	private static void sendHomeMessages(Path subPath, Map<String, String[]> parameters) {
		T.call(AquiletourBackendRequestHandler.class);
	}

	private static void sendLoginMessages(Path path, Map<String, String[]> parameters) {
		T.call(AquiletourBackendRequestHandler.class);

	}

	private static void sendQueueListMessages(Path path, Map<String, String[]> parameters) {
		T.call(AquiletourBackendRequestHandler.class);

	}

	private static void sendQueueMessages(Path path, Map<String, String[]> parameters, User user) {
		T.call(AquiletourBackendRequestHandler.class);
		
		if(path.nameCount() >= 1) {
			sendQueueMenuMessages(parameters, user, path.name(0));
			sendAppointmentMessages(parameters, user, path.name(0));
		}
	}

	private static void sendQueueMenuMessages(Map<String, String[]> parameters, User user, String teacherId) {
		T.call(AquiletourBackendRequestHandler.class);
		
		if(parameters.containsKey("openQueueCourseId")) {
			
			String semesterId  = parameters.get("semesterId")[0];
			String courseId  = parameters.get("openQueueCourseId")[0];
			boolean isQueueOpen = false;
			if(parameters.containsKey("isQueueOpen")
					&& parameters.get("isQueueOpen")[0].equals("on")) {
				isQueueOpen = true;
			}

			UpdateIsQueueOpenMessage updateIsQueueOpenMessage = Ntro.messages().create(UpdateIsQueueOpenMessage.class);
			updateIsQueueOpenMessage.setSemesterId(semesterId);
			updateIsQueueOpenMessage.setTeacherId(teacherId);
			updateIsQueueOpenMessage.setCourseId(courseId);
			updateIsQueueOpenMessage.setIsQueueOpen(isQueueOpen);
			Ntro.messages().send(updateIsQueueOpenMessage);
			
		}else if(parameters.containsKey("openQueueGroupId")) {
			
		}else if(parameters.containsKey("queueMessage")) {
			
			UpdateQueueInfoMessage updateQueueInfoMessage = Ntro.messages().create(UpdateQueueInfoMessage.class);
			
			updateQueueInfoMessage.setTeacherId(user.getId());
			updateQueueInfoMessage.setCourseId(paramValue("courseId", parameters));
			updateQueueInfoMessage.setSemesterId(paramValue("semesterId", parameters));
			updateQueueInfoMessage.setQueueMessage(paramValue("queueMessage", parameters));
			
			Ntro.messages().send(updateQueueInfoMessage);
		}
	}

	private static String paramValue(String paramName, Map<String, String[]> parameters) {
		T.call(AquiletourBackendRequestHandler.class);
		
		String paramValue = null;
		
		if(parameters.containsKey(paramName)) {

			String[] values = parameters.get(paramName);

			if(values.length > 0) {
				paramValue = parameters.get(paramName)[0];
			}
		}
		
		return paramValue;
	}

	private static void sendAppointmentMessages(Map<String, String[]> parameters, User user, String queueId) {
		T.call(AquiletourBackendRequestHandler.class);

		if(parameters.containsKey("makeAppointment")) {
			
			CoursePath coursePath = null;
			TaskPath taskPath = null;
			String taskTitle = null;
			
			if(parameters.containsKey("coursePath")) {
				coursePath = CoursePath.fromKey(parameters.get("coursePath")[0]);
			}

			if(parameters.containsKey("taskPath")) {
				taskPath = TaskPath.fromKey(parameters.get("taskPath")[0]);
			}

			if(parameters.containsKey("taskTitle")) {
				taskTitle = parameters.get("taskTitle")[0];
			}

			AddAppointmentMessage addAppointmentMessage = Ntro.messages().create(AddAppointmentMessage.class);
			addAppointmentMessage.setQueueId(queueId);
			addAppointmentMessage.setCoursePath(coursePath);
			addAppointmentMessage.setTaskPath(taskPath);
			addAppointmentMessage.setTaskTitle(taskTitle);
			Ntro.messages().send(addAppointmentMessage);
			
		} else if(parameters.containsKey("deleteAppointment")){
			
			DeleteAppointmentMessage deleteAppointmentMessage = Ntro.messages().create(DeleteAppointmentMessage.class);
			String appointmentId = parameters.get("deleteAppointment")[0];
			deleteAppointmentMessage.setAppointmentId(appointmentId);
			deleteAppointmentMessage.setCourseId(queueId);
			Ntro.messages().send(deleteAppointmentMessage);
			

		} else if(parameters.containsKey("move")) {

			String appointmentId = parameters.get("move")[0];
			String destinationId = null;
			String beforeOrAfter = null;
			if(parameters.containsKey("after")) {
				destinationId = parameters.get("after")[0];
				beforeOrAfter = "after";
			}else if(parameters.containsKey("before")) {
				destinationId = parameters.get("before")[0];
				beforeOrAfter = "before";
			}

			MoveAppointmentMessage moveAppointmentMessage = Ntro.messages().create(MoveAppointmentMessage.class);
			moveAppointmentMessage.setCourseId(queueId);
			moveAppointmentMessage.setAppointmentId(appointmentId);
			moveAppointmentMessage.setDestinationId(destinationId);
			moveAppointmentMessage.setBeforeOrAfter(beforeOrAfter);
			Ntro.messages().send(moveAppointmentMessage);

		} else if(parameters.containsKey("decrementAppointmentTimes")) {
			
			sendModifyAppointmentTimesMessage(-Constants.APPOINTMENT_TIME_INCREMENT_SECONDS);
			
		} else if(parameters.containsKey("incrementAppointmentTimes")) {

			sendModifyAppointmentTimesMessage(+Constants.APPOINTMENT_TIME_INCREMENT_SECONDS);

		} else if(parameters.containsKey("decreaseAppointmentDuration")) {
			
			sendModifyAppointmentDurationsMessage(-Constants.APPOINTMENT_DURATION_INCREMENT_SECONDS);

		} else if(parameters.containsKey("increaseAppointmentDuration")) {

			sendModifyAppointmentDurationsMessage(+Constants.APPOINTMENT_DURATION_INCREMENT_SECONDS);

		} else if(parameters.containsKey("modifyCommentForQueueId")) {
			
			String comment = parameters.get("comment")[0];

			ModifyAppointmentCommentMessage modifyAppointmentComment = Ntro.messages().create(ModifyAppointmentCommentMessage.class);
			modifyAppointmentComment.setQueueId(queueId);
			modifyAppointmentComment.setComment(comment);
			Ntro.messages().send(modifyAppointmentComment);
		}
	}


	private static void sendModifyAppointmentDurationsMessage(int incrementSeconds) {
		T.call(AquiletourBackendRequestHandler.class);

		ModifyAppointmentDurationsMessage modifyAppointmentDurations = Ntro.messages().create(ModifyAppointmentDurationsMessage.class);
		modifyAppointmentDurations.setDurationIncrementSeconds(incrementSeconds);
		Ntro.messages().send(modifyAppointmentDurations);
	}

	private static void sendModifyAppointmentTimesMessage(int incrementSeconds) {
		T.call(AquiletourBackendRequestHandler.class);

		ModifyAppointmentTimesMessage modifyAppointmentTimes = Ntro.messages().create(ModifyAppointmentTimesMessage.class);
		modifyAppointmentTimes.setTimeIncrementSeconds(incrementSeconds);
		Ntro.messages().send(modifyAppointmentTimes);
	}

}
