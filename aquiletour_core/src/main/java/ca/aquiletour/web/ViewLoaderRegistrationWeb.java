package ca.aquiletour.web;

import ca.aquiletour.core.pages.home.HomeView;
import ca.aquiletour.core.pages.login.LoginView;
import ca.aquiletour.core.pages.queue.student.views.AppointmentViewStudent;
import ca.aquiletour.core.pages.queue.student.views.QueueViewStudent;
import ca.aquiletour.core.pages.queue.teacher.views.AppointmentViewTeacher;
import ca.aquiletour.core.pages.queue.teacher.views.QueueViewTeacher;
import ca.aquiletour.core.pages.queue_list.views.QueueListItemView;
import ca.aquiletour.core.pages.queue_list.views.QueueListView;
import ca.aquiletour.core.pages.root.RootView;
import ca.aquiletour.web.pages.home.HomeViewWeb;
import ca.aquiletour.web.pages.login.LoginViewWeb;
import ca.aquiletour.web.pages.queue.student.AppointmentViewWebStudent;
import ca.aquiletour.web.pages.queue.student.QueueViewWebStudent;
import ca.aquiletour.web.pages.queue.teacher.AppointmentViewWebTeacher;
import ca.aquiletour.web.pages.queue.teacher.QueueViewWebTeacher;
import ca.aquiletour.web.pages.queues.QueueListItemViewWeb;
import ca.aquiletour.web.pages.queues.QueueListViewWeb;
import ca.aquiletour.web.pages.root.RootViewWeb;
import ca.ntro.core.mvc.ViewLoaders;
import ca.ntro.core.system.trace.T;
import ca.ntro.services.Ntro;

public class ViewLoaderRegistrationWeb {
	
	public static void registerViewLoaders() {
		T.call(ViewLoaderRegistrationWeb.class);
		
		ViewLoaders.registerViewLoader(RootView.class,
				"fr"
				, Ntro.viewLoaderWeb()
			     	.setHtmlUrl("/views/root/root.html")
			     	.setCssUrl("/views/root/root.css")
			     	.setTranslationsUrl("/i18n/fr/string.json")
			     	.setTargetClass(RootViewWeb.class));

		ViewLoaders.registerViewLoader(QueueViewStudent.class,
				"fr"
				, Ntro.viewLoaderWeb()
				.setHtmlUrl("/views/queue/student/queue_student.html")
				.setCssUrl("/views/queue/student/queue_student.css")
				.setTranslationsUrl("/i18n/fr/string.json")
				.setTargetClass(QueueViewWebStudent.class));
		
		ViewLoaders.registerViewLoader(QueueViewTeacher.class,
				"fr"
				, Ntro.viewLoaderWeb()
			     	.setHtmlUrl("/views/queue/teacher/queue_teacher.html")
			     	.setCssUrl("/views/queue/teacher/queue_teacher.css")
			     	.setTranslationsUrl("/i18n/fr/string.json")
			     	.setTargetClass(QueueViewWebTeacher.class));

		ViewLoaders.registerViewLoader(QueueListView.class,
				"fr"
				, Ntro.viewLoaderWeb()
			     	.setHtmlUrl("/views/queue_list/queue_list.html")
			     	.setCssUrl("/views/queue_list/queue_list.css")
			     	.setTranslationsUrl("/i18n/fr/string.json")
			     	.setTargetClass(QueueListViewWeb.class));

		ViewLoaders.registerViewLoader(QueueListItemView.class,
				"fr"
				, Ntro.viewLoaderWeb()
			     	.setHtmlUrl("/partials/queue_list_item/queue_list_item.html")
			     	.setCssUrl("/partials/queue_list_item/queue_list_item.css")
			     	.setTranslationsUrl("/i18n/fr/string.json")
			     	.setTargetClass(QueueListItemViewWeb.class));

		ViewLoaders.registerViewLoader(AppointmentViewTeacher.class,
				"fr"
				, Ntro.viewLoaderWeb()
				.setHtmlUrl("/partials/appointment/teacher/appointment_teacher.html")
				.setCssUrl("/partials/appointment/teacher/appointment_teacher.css")
				.setTranslationsUrl("/i18n/fr/string.json")
				.setTargetClass(AppointmentViewWebTeacher.class));
		
		ViewLoaders.registerViewLoader(AppointmentViewStudent.class,
				"fr"
				, Ntro.viewLoaderWeb()
			     	.setHtmlUrl("/partials/appointment/student/appointment_student.html")
			     	.setCssUrl("/partials/appointment/student/appointment_student.css")
			     	.setTranslationsUrl("/i18n/fr/string.json")
			     	.setTargetClass(AppointmentViewWebStudent.class));

		ViewLoaders.registerViewLoader(LoginView.class,
				"fr"
				, Ntro.viewLoaderWeb()
				.setHtmlUrl("/views/login/login.html")
				.setCssUrl("/views/login/login.css")
				.setTranslationsUrl("/i18n/fr/string.json")
				.setTargetClass(LoginViewWeb.class));
		
		ViewLoaders.registerViewLoader(HomeView.class,
				"fr"
				, Ntro.viewLoaderWeb()
			     	.setHtmlUrl("/views/home/home.html")
			     	.setCssUrl("/views/home/home.css")
			     	.setTranslationsUrl("/i18n/fr/string.json")
			     	.setTargetClass(HomeViewWeb.class));
	}
}
