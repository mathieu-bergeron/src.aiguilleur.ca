package ca.aquiletour.web;

import java.util.Map;

import ca.aquiletour.core.Constants;
import ca.aquiletour.core.messages.course.CourseTaskMessage;
import ca.aquiletour.core.messages.user.ShowPasswordMenu;
import ca.aquiletour.core.messages.user.UserIsActiveMessage;
import ca.aquiletour.core.models.paths.TaskPath;
import ca.aquiletour.core.models.session.SessionData;
import ca.aquiletour.core.models.user.Teacher;
import ca.aquiletour.core.models.user.User;
import ca.aquiletour.core.pages.home.ShowHomeMessage;
import ca.aquiletour.core.pages.login.ShowLoginMessage;
import ca.aquiletour.core.pages.queue.student.messages.ShowQueueMessageStudent;
import ca.aquiletour.core.pages.queue.teacher.messages.ShowQueueMessageTeacher;
import ca.aquiletour.core.pages.queue_list.messages.ShowQueueListMessage;
import ca.ntro.core.Path;
import ca.ntro.core.mvc.NtroContext;
import ca.ntro.core.system.trace.T;
import ca.ntro.services.Ntro;

public class AquiletourRequestHandler {
	
	public static void sendMessages(NtroContext<User, SessionData> context, Path path, Map<String, String[]> parameters) {
		T.call(AquiletourRequestHandler.class);
		
		// XXX: register that user is active for every action
		Ntro.backendService().sendMessageToBackend(Ntro.messages().create(UserIsActiveMessage.class));

		sendRootMessages(context, path, parameters);
		
		if(path.startsWith(Constants.QUEUE_LIST_URL_SEGMENT)) {
			
			sendQueuesMessages(path.subPath(1), parameters);

		}else if(path.startsWith(Constants.QUEUE_URL_SEGMENT)) {
			
			sendQueueMessages(path.subPath(1), parameters, context.user());

		}else if(path.startsWith(Constants.LOGIN_URL_SEGMENT)) {
			
			sendLoginMessages(path.subPath(1), parameters);
			
		} else if(path.startsWith(Constants.HOME_URL_SEGMENT)) {
			
			sendHomeMessages(path.subPath(1), parameters);

		} else if(path.startsWith(Constants.LOGOUT_URL_SEGMENT)) {

			Ntro.messages().send(Ntro.messages().create(ShowHomeMessage.class));

		}
	}

	public static void sendRootMessages(NtroContext<User, SessionData> context, Path path, Map<String, String[]> parameters) {
		T.call(AquiletourRequestHandler.class);

		if(parameters.containsKey("showPasswordMenu")) {
			
			ShowPasswordMenu showPasswordMenu = Ntro.messages().create(ShowPasswordMenu.class);
			Ntro.messages().send(showPasswordMenu);
		}
	}

	private static void sendHomeMessages(Path subPath, Map<String, String[]> parameters) {
		T.call(AquiletourRequestHandler.class);

		ShowHomeMessage showHomeMessage = Ntro.messages().create(ShowHomeMessage.class);
		Ntro.messages().send(showHomeMessage);
	}

	private static void sendLoginMessages(Path path, Map<String, String[]> parameters) {
		T.call(AquiletourRequestHandler.class);
		
		ShowLoginMessage showLoginMessage = Ntro.messages().create(ShowLoginMessage.class);
		
		if(parameters.containsKey("message")) {
			showLoginMessage.setMessageToUser(parameters.get("message")[0]);
		}

		Ntro.messages().send(showLoginMessage);
	}

	private static void sendQueuesMessages(Path path, Map<String, String[]> parameters) {
		T.call(AquiletourRequestHandler.class);

		ShowQueueListMessage showQueuesMessage = Ntro.messages().create(ShowQueueListMessage.class);
		Ntro.messages().send(showQueuesMessage);
	}
	
	static <MSG extends CourseTaskMessage> MSG createCourseTaskMessage(Class<MSG> messageClass, 
			                                                   Path path, 
			                                                   Map<String, String[]> parameters,
			                                                   SessionData sessionData) {
		T.call(AquiletourRequestHandler.class);
		
		MSG message = Ntro.messages().create(messageClass);

		String teacherId = path.name(0);
		String semesterId = path.name(1);
		String courseId = path.name(2);
		TaskPath taskPath = TaskPath.fromPath(path.subPath(3));

		message.setTeacherId(teacherId);
		message.setCourseId(courseId);
		message.setSemesterId(semesterId);

		if(taskPath.nameCount() > 0) {
			message.setTaskPath(taskPath);
		}
		
		return message;
	}

	private static void sendQueueMessages(Path path, Map<String, String[]> parameters, User user) {
		T.call(AquiletourRequestHandler.class);
		
		if(path.nameCount() >= 1) {//TODO 

			String teacherId = path.name(0);


			if(user instanceof Teacher 
					&& user.getId().equals(teacherId)
					&& user.actsAsTeacher()) {

				ShowQueueMessageTeacher showTeacherQueueMessage = Ntro.messages().create(ShowQueueMessageTeacher.class);
				showTeacherQueueMessage.setQueueId(teacherId);
				Ntro.messages().send(showTeacherQueueMessage);

			}else {
				
				ShowQueueMessageStudent showStudentQueueMessage = Ntro.messages().create(ShowQueueMessageStudent.class);
				showStudentQueueMessage.setQueueId(teacherId);
				Ntro.messages().send(showStudentQueueMessage);
			}
		}
	}

}
