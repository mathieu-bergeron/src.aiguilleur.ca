# Aiguilleur.ca (Aquiletour)

Forked from: https://github.com/mathieu-bergeron/aquiletour2021

## Dependencies

* JDK11 (<= JDK15)
* nodejs
* optionnal: tsc (typescript compiler)

## Building

* Compile Javascript once

    ```bash
    $ sh gradlew js
    ```

## Running

* Run server as localhost:8080

    ```bash
    $ sh gradlew server
    ```

* Visit <a href="http://localhost:8080" target="_blank">localhost:8080</a>

* Display codes that would be sent by email

    ```bash
    $ tail -f aquiletour_server/email_codes.txt
    ```


## Developping

* Eclipse projects

    ```
    $ sh gradlew eclipse --continue
    ```

* Recompile Javascript when needed

    ```
    $ sh gradlew js
    ```

* Restart server when needed

    ```
    $ sh gradlew server
    ```

