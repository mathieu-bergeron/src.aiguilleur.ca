$(document).ready(function(){
    initializeWidgets($(document));

    // XXX: avoid form resubmission on refresh
    //      https://www.webtrickshome.com/faq/how-to-stop-form-resubmission-on-page-refresh
    if (window.history.replaceState) {
        window.history.replaceState( null, null, window.location.href );
    }
});

function initializeView(viewName, viewRootElement, jSweet){

    if(viewName === "RootViewWeb"){

        initializeRoot(viewRootElement, jSweet);

    }else if(viewName === "QueueViewWebTeacher"){

        initializeQueue(viewRootElement, jSweet);
        initializeQueueTeacher(viewRootElement, jSweet);
        initializeCheckboxes(viewRootElement);
        initializeCollapse(viewRootElement);
        initializeClickBlocker(viewRootElement);
        initializeDropdownMenus(viewRootElement);

    }
}

