package ca.aquiletour.server.backend;


import ca.aquiletour.core.messages.AddStudentCsvMessage;
import ca.aquiletour.core.messages.InitializeSessionMessage;
import ca.aquiletour.core.messages.queue.UpdateIsQueueOpenMessage;
import ca.aquiletour.core.messages.queue.UpdateQueueInfoMessage;
import ca.aquiletour.core.messages.time.TimePassesMessage;
import ca.aquiletour.core.messages.user.ItsNotMeMessage;
import ca.aquiletour.core.messages.user.RenameUserMessage;
import ca.aquiletour.core.messages.user.ToggleAdminModeMessage;
import ca.aquiletour.core.messages.user.ToggleStudentModeMessage;
import ca.aquiletour.core.messages.user.UpdateUserInfoMessage;
import ca.aquiletour.core.messages.user.UserChangesPasswordMessage;
import ca.aquiletour.core.messages.user.UserInitiatesLoginMessage;
import ca.aquiletour.core.messages.user.UserIsActiveMessage;
import ca.aquiletour.core.messages.user.UserLogsOutMessage;
import ca.aquiletour.core.messages.user.UserSendsLoginCodeMessage;
import ca.aquiletour.core.messages.user.UserSendsPasswordMessage;
import ca.aquiletour.core.pages.queue.messages.ModifyAppointmentDurationsMessage;
import ca.aquiletour.core.pages.queue.messages.ModifyAppointmentTimesMessage;
import ca.aquiletour.core.pages.queue.student.messages.AddAppointmentMessage;
import ca.aquiletour.core.pages.queue.student.messages.ModifyAppointmentCommentMessage;
import ca.aquiletour.core.pages.queue.teacher.messages.DeleteAppointmentMessage;
import ca.aquiletour.core.pages.queue.teacher.messages.MoveAppointmentMessage;
import ca.aquiletour.server.backend.login.InitializeSessionHandler;
import ca.aquiletour.server.backend.login.ItsNotMeHandler;
import ca.aquiletour.server.backend.login.UserInitiatesLoginHandler;
import ca.aquiletour.server.backend.login.UserSendsLoginCodeHandler;
import ca.aquiletour.server.backend.login.UserSendsPasswordHandler;
import ca.aquiletour.server.backend.login.UserLogsOutHandler;
import ca.aquiletour.server.backend.queue.AddAppointmentHandler;
import ca.aquiletour.server.backend.queue.DeleteAppointmentHandler;
import ca.aquiletour.server.backend.queue.ModifyAppointmentCommentHandler;
import ca.aquiletour.server.backend.queue.ModifyAppointmentDurationsHandler;
import ca.aquiletour.server.backend.queue.ModifyAppointmentTimesHandler;
import ca.aquiletour.server.backend.queue.MoveAppointmentHandler;
import ca.aquiletour.server.backend.queue.UpdateIsQueueOpenHandler;
import ca.aquiletour.server.backend.queue.UpdateQueueInfoHandler;
import ca.aquiletour.server.backend.time.TimePassesHandler;
import ca.aquiletour.server.backend.users.AddStudentCsvHandler;
import ca.aquiletour.server.backend.users.RenameUserHandler;
import ca.aquiletour.server.backend.users.ToggleAdminModeHandler;
import ca.aquiletour.server.backend.users.ToggleStudentModeHandler;
import ca.aquiletour.server.backend.users.UpdateUserInfoHandler;
import ca.aquiletour.server.backend.users.UserChangesPasswordHandler;
import ca.aquiletour.server.backend.users.UserIsActiveHandler;
import ca.aquiletour.server.registered_sockets.RegisteredSocketsSockJS;
import ca.ntro.core.system.trace.T;
import ca.ntro.jdk.services.BackendServiceServer;
import ca.ntro.services.Ntro;
import ca.ntro.users.NtroUser;

public class AquiletourBackendService extends BackendServiceServer {
	
	@Override
	protected void addBackendMessageHandlers() {

		addBackendMessageHandler(AddAppointmentMessage.class, new AddAppointmentHandler());
		addBackendMessageHandler(AddStudentCsvMessage.class, new AddStudentCsvHandler());
		addBackendMessageHandler(DeleteAppointmentMessage.class, new DeleteAppointmentHandler());
		addBackendMessageHandler(MoveAppointmentMessage.class, new MoveAppointmentHandler());
		addBackendMessageHandler(UserInitiatesLoginMessage.class, new UserInitiatesLoginHandler());
		addBackendMessageHandler(InitializeSessionMessage.class, new InitializeSessionHandler());
		addBackendMessageHandler(UserSendsLoginCodeMessage.class, new UserSendsLoginCodeHandler());
		addBackendMessageHandler(UserLogsOutMessage.class, new UserLogsOutHandler());
		addBackendMessageHandler(UpdateUserInfoMessage.class, new UpdateUserInfoHandler());
		addBackendMessageHandler(TimePassesMessage.class, new TimePassesHandler());
		addBackendMessageHandler(ToggleStudentModeMessage.class, new ToggleStudentModeHandler());
		addBackendMessageHandler(ModifyAppointmentDurationsMessage.class, new ModifyAppointmentDurationsHandler());
		addBackendMessageHandler(ModifyAppointmentTimesMessage.class, new ModifyAppointmentTimesHandler());
		addBackendMessageHandler(ModifyAppointmentCommentMessage.class, new ModifyAppointmentCommentHandler());
		addBackendMessageHandler(ItsNotMeMessage.class, new ItsNotMeHandler());
		addBackendMessageHandler(UserChangesPasswordMessage.class, new UserChangesPasswordHandler());
		addBackendMessageHandler(UserSendsPasswordMessage.class, new UserSendsPasswordHandler());
		addBackendMessageHandler(ToggleAdminModeMessage.class, new ToggleAdminModeHandler());
		addBackendMessageHandler(UpdateIsQueueOpenMessage.class, new UpdateIsQueueOpenHandler());
		addBackendMessageHandler(UpdateQueueInfoMessage.class, new UpdateQueueInfoHandler());
		addBackendMessageHandler(UserIsActiveMessage.class, new UserIsActiveHandler());
		addBackendMessageHandler(RenameUserMessage.class, new RenameUserHandler());
	}

	@Override
	protected void beforeCallingHandler(NtroUser requestingUser) {
		T.call(this);

		RegisteredSocketsSockJS.createInvokeValueMessageQueue(Ntro.threadService().currentThread().threadId(), requestingUser);
	}

	@Override
	protected void afterCallingHandler(NtroUser requestingUser) {
		T.call(this);

		RegisteredSocketsSockJS.flushInvokeValueMessageQueue(Ntro.threadService().currentThread().threadId(), requestingUser);
	}
}
