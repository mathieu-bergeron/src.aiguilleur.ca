package ca.aquiletour.server.backend.queue;

import java.util.ArrayList;
import java.util.List;

import ca.aquiletour.core.models.user.User;
import ca.aquiletour.core.pages.queue.teacher.messages.DeleteAppointmentMessage;
import ca.aquiletour.core.pages.root.messages.ShowLoginMenuMessage;
import ca.aquiletour.server.backend.queue_list.QueueListManager;
import ca.ntro.backend.BackendError;
import ca.ntro.backend.BackendMessageHandler;
import ca.ntro.core.system.trace.T;
import ca.ntro.messages.NtroMessage;
import ca.ntro.services.ModelStoreSync;
import ca.ntro.services.Ntro;

public class DeleteAppointmentHandler extends BackendMessageHandler<DeleteAppointmentMessage> {
	
	@Override
	public void handleNow(ModelStoreSync modelStore,DeleteAppointmentMessage message) throws BackendError {
		T.call(this);
		
		User user = message.getUser();
		String userId = user.getId();
		String courseId = message.getCourseId();
		String appointmentId = message.getAppointmentId();

		if(user.isGuest()) {

			List<NtroMessage> delayedMessages = new ArrayList<>();
			delayedMessages.add(message);

			ShowLoginMenuMessage showLoginMenuMessage = Ntro.messages().create(ShowLoginMenuMessage.class);
			showLoginMenuMessage.setMessageToUser("SVP se connecter pour effacer le message");
			showLoginMenuMessage.setDelayedMessages(delayedMessages);
			Ntro.messages().send(showLoginMenuMessage);

		}else {
			
			if(courseId.equals(user.getId())) {

				QueueManager.deleteAppointmentAsTeacher(modelStore, courseId, appointmentId, userId);
				
			}else {

				QueueManager.deleteAppointmentAsStudent(modelStore, courseId, appointmentId, userId);

			}
		}

	}

	@Override
	public void handleLater(ModelStoreSync modelStore, DeleteAppointmentMessage message) throws BackendError {
		T.call(this);
		
		String queueId = message.getCourseId();
		String appointmentId = message.getAppointmentId();

		QueueListManager.updateLastActivity(modelStore, queueId);
	}
}
