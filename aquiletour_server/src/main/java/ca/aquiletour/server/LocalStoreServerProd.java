package ca.aquiletour.server;

import java.util.List;

import ca.ntro.core.json.JsonLoader;
import ca.ntro.core.models.NtroModel;
import ca.ntro.core.system.trace.T;
import ca.ntro.messages.NtroModelMessage;
import ca.ntro.stores.DocumentPath;
import ca.ntro.stores.ValuePath;

public class LocalStoreServerProd extends LocalStoreMongoDbServer {

	private LocalStoreCommon localStoreCommon = new LocalStoreCommon();

	@Override
	public JsonLoader getJsonLoader(Class<? extends NtroModel> targetClass, DocumentPath documentPath) {
		T.call(this);

		JsonLoader jsonLoader = localStoreCommon.getJsonLoader(targetClass, documentPath);
		
		if(jsonLoader == null) {

			jsonLoader = super.getJsonLoader(targetClass, documentPath);

		}
		
		return jsonLoader;
	}
	
	@Override
	public void onValueMethodInvoked(ValuePath valuePath, String methodName, List<Object> args) {
		T.call(this);

		localStoreCommon.onValueMethodInvoked(valuePath, methodName, args);
	}

	@Override
	protected JsonLoader jsonLoaderFromRequest(String serviceUrl, NtroModelMessage message) {
		T.call(this);
		
		return localStoreCommon.jsonLoaderFromRequest(serviceUrl, message);
	}

	@Override
	protected int maxHeapSize() {
		T.call(this);

		return localStoreCommon.maxHeapSize();
	}
}
