package ca.ntro.services;

public class ConfigService {

	public boolean isProd() {
		// JSWEET: makes this easy to change in bundle.js
		boolean isProdDefaultValue = true;
		return isProdDefaultValue;
	}

	public boolean useHttps() {
		return false;
	}

	public boolean useSslWebSocket() {
		return false;
	}

	public boolean useMongoDb() {
		return false;
	}

	public boolean useProdLogging() {
		// JSWEET: makes this easy to change in bundle.js
		boolean useProdLoggingDefaultValue = true;
		return useProdLoggingDefaultValue;
	}
}
